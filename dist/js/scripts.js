//preloader func
var preloader = function (elem) {
    elem.find('img').fadeOut('slow').end().delay(400).fadeOut('slow');
};
var countSlide = function (val) {
    var slidesCount = $(val).find('.slick-slide:not(.slick-cloned)').length;
    $(val).find('.amount').html(slidesCount);
    var current = $(val).find('.slick-slide.slick-current').attr('data-slick-index');
    var active = $(val).find('.slick-slide.slick-active').length;
    var viewed = Number(current) + ((active > 1) ? active : 1)
    $(val).find('.num').html((viewed > slidesCount) ? slidesCount : viewed);

    // if ($(val).is(".review__b")) {
    //     var active = $(val).find('.slick-active').length;
    //     var current = $(val).find('.slick-current').attr('data-slick-index');
    //     var itemsCount = $(val).find('.slick-slide:not(.slick-cloned)').length;
    //     $(val).find('.amount').html(itemsCount);
    //     $(val).find('.num').html(active);
    // }
};
var closePopup = function (el) {
    $(el).fadeOut();
    $('body').removeClass('_modal-open');
};
/**team slider func */
var teamSlider = function () {
    if (!$('.team__list').hasClass("slick-initialized") && $(window).width() < 1349) {
        $('.team__list').slick({
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 2,
            slidesToScroll: 2,
            arrows: false,
            responsive: [{
                    breakpoint: 1349,
                    settings: {
                        dots: false,
                        infinite: false,
                        speed: 300,
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        arrows: false
                    }
                },
                {
                    breakpoint: 720,
                    settings: {
                        dots: false,
                        infinite: false,
                        speed: 300,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false
                    }
                }
            ]
        });
    } else if ($('.team__list').hasClass("slick-initialized") && $(window).width() >= 1349) {
        $('.team__list').slick('unslick');
    }
};

/**
 * slider init
 */
var projectSlider = function (elem) {
    var sliderList = $(elem);
    if (!sliderList.hasClass("slick-initialized")) {
        sliderList.each(function () {
            $(this).slick({
                dots: false,
                infinite: true,
                speed: 300,
                slidesToShow: 1,
                fade: true,
                arrows: false,
                responsive: [{
                    breakpoint: 1024,
                    settings: {
                        dots: true
                    }
                }]
            });
        });
    }
}
/**
 * slider img preload
 */
var imgOnLoad = function (img) {
    var slider = $('.slider');
    if (img[0].complete || img[0].readyState === 4) {
        preloader($(img).parents(slider).find('.preloader'));
    } else {
        $(img).on("load", function () {
            preloader($(img).parents(slider).find('.preloader'));
        });
    }
}
/**review slider init*/
var reviewSlider = function () {
    if (!$(".review__list").hasClass("slick-initialized")) {
        $(".review__list").each(function () {
            $(this).slick({
                dots: false,
                infinite: false,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 4,
                arrows: false,
                responsive: [{
                        breakpoint: 1023,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                        }
                    },
                    {
                        breakpoint: 720,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    }
                ]
            });
        });
    }
}
$(function () {
    projectSlider($(".slider__list"));
    reviewSlider();
    featuresSlider();

    var slider = $('.slider');
    /**
     * slider img preload
     */
    var img = slider.find('.slider__item img:first');
    $.each(img, function () {
        imgOnLoad($(this));
    });
    /**
     * slides count
     */
    $.each(slider, function (index, val) {
        countSlide(val);
        $(val).on('afterChange', function (event, slick, currentSlide, nextSlide) {
            countSlide(val);
        });
    });
    /**
     * ctrl slider
     */
    $('body').on('click', '[data-attr=prevSlide], [data-attr=nextSlide]', function () {
        if ($(this).is("[data-attr=prevSlide]")) {
            $(this).parents('.slider').find('.slick-slider').slick("slickPrev");
        } else if ($(this).is("[data-attr=nextSlide]")) {
            $(this).parents('.slider').find('.slick-slider').slick("slickNext");
        }
        return false;
    });
    /**
     * modal
     */
    $('body').on('click', '.js-modal', function (event) {
        event.preventDefault();
        var modal = $(this).attr('data-target');
        $('[data-id="' + modal + '"').fadeIn();
        $('[data-id="' + modal + '"').find('input').focus();
        $('body').addClass('_modal-open');
    });
    $(document).click(function (e) {
        if ($('.modal').is(e.target) && $('.modal').has(e.target).length === 0) {
            closePopup(e.target);
        };
    });
    /**
     * team slider
     */
    teamSlider();
    countSlide(".team");
    $(".team__list").on('afterChange', function (event, slick, currentSlide, nextSlide) {
        countSlide(".team");
    });
    /**
     * window resize
     */
    $(window).resize(function () {
        teamSlider();
        countSlide(".team");
        featuresSlider();
        countSlide(".features");
    });
    /**
     * ajax more projects
     */
    $('body').on('click', '.js-more', function (e) {
        e.preventDefault();
        var numOfShowElem = 2;
        var currentElem = $('.projects').find('.favproject').length;
        console.log(currentElem);
        $.ajax({
            type: "get",
            url: "./ui.html",
            dataType: "html",
            success: function (res) {
                var item = $(res).find('.favproject');
                $.each(item, function (i, val) {
                    var img = $(this).find('.slider__item img:first');
                    var slider = $(this).find(".slider__list");
                    var sliderWrap = $(this).find(".slider");
                    if (currentElem - 1 < i && i < currentElem + numOfShowElem) {
                        $(this).appendTo($('.projects__list'));
                        projectSlider(slider);
                        imgOnLoad(img);
                        countSlide(sliderWrap);
                        $(sliderWrap).on('afterChange', function (event, slick, currentSlide, nextSlide) {
                            countSlide(sliderWrap);
                        });
                    }
                });
            }
        });
    });
    /**active link*/
    $(window).scroll(function () {
        var $sections = $('.section');
        $sections.each(function (i, el) {
            var top = $(el).offset().top - 10;
            var bottom = top + $(el).height();
            var scroll = $(window).scrollTop();
            var id = $(el).attr('id');
            if (scroll > top && scroll < bottom) {
                $('.js-anchor').removeClass('active');
                $('a[href="#' + id + '"]').addClass('active');
            }
        });
    });

    /**
     * fixed header
     * fixed sidebar
     */
    var sidebar = $('.sidebar');
    var offset = $(sidebar).offset();
    var top = offset.top;
    $(window).scroll(function () {
        var windowTop = $(window).scrollTop();
        var header = $('.header__menu.mobile');

        // if (windowTop > top) {
        //     sidebar.css({
        //         "position": "fixed"
        //     });
        // } else {
        //     sidebar.css({
        //         "position": "absolute"
        //     });
        // }

        if (windowTop > header.height()) {
            header.fadeIn();
        } else {
            header.fadeOut();
        }
    });

    /**scroll*/
    $('.js-anchor').bind('click', function (event) {
        var $anchor = $(this);
        var $workarea = 0;
        if ($(window).width() < 1366) {
            var $workarea = $('.workarea').offset().top - 4;
        }

        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - $workarea
        }, 1000);
        event.preventDefault();
    });

    /**
     * youtube mask and play
     */
    $('.js-youtube-video').click(function () {
        var $t = $(this);

        $t.after($('<iframe width="100%" height="100%" src="' + $t.data('src') + '?autoplay=1" frameborder="0" allowfullscreen autoplay></iframe>'));
        $t.remove();
    });
    /**
     * toggle sidebar
     * close sidebar
     */
    $('body').on('click', '.js-toggle', function (e) {
        e.preventDefault();
        var elemID = $(this).data('target');
        $('[data-id=' + elemID + ']').addClass('is-open').animate({
            left: "+=100%"
        }, 600);
    });
    $('body').on('click', '.sidebar .close, .sidebar__link', function (e) {
        closeSidebar();
    });
    $(window).resize(function () {
        closeSidebar();
        if ($(window).width() >= 1007) {
            $('.rate__head').removeClass('active');
            $('.rate__body').removeAttr('style');
        }
        if ($(window).width() < 1007) {
            $('.rate__body:visible').parent().find('.rate__head').addClass('active');
        }
    });
    /**
     * toggle price block
     */
    $('body').on('click', '.js-toggle-slide', function (e) {
        e.preventDefault();
        var el = $(this);
        if ($(window).width() < 1007) {
            $(el).next('[data-attr="tab"]').slideToggle('slow')
                .parent()
                .siblings()
                .find('[data-attr="tab"]:visible')
                .slideUp('slow');
            el.toggleClass('active');
            el.parent()
                .siblings()
                .find('[data-toggle]').removeClass('active');
        }
    });
    /**
     * form validation
     */
    $("form").submit(function (e) {
        e.preventDefault();

        var size = 0;
        var form = $(this);
        var inp = form.find('.required input');
        var popup = $('.modal');
        var errorMsg = 'Обязательно заполните';
        var errorPopup = $('[data-id="modalError"]');
        var id = errorPopup.attr("data-for");

        form.find('.required').removeClass('error');
        form.find('.error__msg').remove();

        if (form.is($('#formError'))) {

            if (inp.is('[type=tel]')) {
                if (inp.val() == "") {
                    size += 1;
                } else {
                    $('[id=' + id + ']').find('[type=tel]').val($(inp).val());
                    closePopup(popup);
                    $('[id=' + id + ']').parents('.modal').show();
                    errorPopup.attr("data-for", "");
                }
            }
            return false;
        } else {
            inp.each(function () {
                if ($(this).is('[type=email]')) {
                    if ($(this).val() === "") {
                        $(this).parent().addClass('error');
                        $(this).attr('placeholder', errorMsg);
                        size += 1;
                    } else {
                        $(this).attr('placeholder', 'E-mail');
                    }
                }
                if ($(this).is('[type=tel]')) {
                    if ($(this).val() == "") {
                        popup.hide();
                        errorPopup.attr("data-for", '"' + form.attr('id') + '"').show();
                        size += 1;
                    } else {
                        size = size + checkPhone($(this));
                    }
                }
            });
        }
        if (size > 0) {
            return false;
        };
        $.ajax({
            url: form.attr('action'),
            data: form.serialize(),
            success: function () {
                $('input').each(function () {
                    $(this).val('').parent().removeClass('error');
                });
                popup.fadeOut();
                $('[data-id="modalSuccess"]').fadeIn();
                console.log('success');
            }
        });
    });
    $('body').on('click', '.cgroup__item a', function (e) {
        e.preventDefault();
        var wrap = $(this).parent();
        var inp = wrap.parents('.cgroup').find('[name="SERVICES"]');
        wrap.siblings().removeClass('_checked');
        wrap.addClass('_checked');
        inp.val($(this).attr('data-value'));                
    });
});

function closeSidebar() {
    if ($("[data-id=sidebar]").hasClass('is-open')) {
        $("[data-id=sidebar]").animate({
            left: "-=100%"
        }, 600);
        $("[data-id=sidebar]").queue(function () {
            $(this).removeClass('is-open');
            $(this).removeAttr('style');
            $(this).dequeue();
        });
    }
}
/*check EMAIL*/
function checkEmail(emailItem) {
    var emailItemValue = emailItem.val();
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{1,3})$/;
    if (!reg.test(emailItemValue)) {
        emailItem.parent().addClass("error");
        return false;
    } else {
        emailItem.parent().removeClass("error");
        return true;
    }
}
/*check PHONE*/
function checkPhone(phone) {
    var phoneVal = phone.val();
    var reg = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;

    if (!reg.test(phoneVal)) {
        phone.parent().addClass("error");
        phone.parent().append('<p class="error__msg" style="color: red;">Некорректный номер</p>');
        return 1;
    } else {
        phone.parent().removeClass("error");
        return 0;
    }
}
/**features slider func */
function featuresSlider() {
    if (!$('.features__list').hasClass("slick-initialized") && $(window).width() < 720) {
        $('.features__list').slick({
            dots: false,
            infinite: false,
            speed: 1,
            slidesToShow: 3,
            slidesToScroll: 3,
            arrows: false,
            vertical: true
        });
    } else if ($('.features__list').hasClass("slick-initialized") && $(window).width() >= 720) {
        $('.features__list').slick('unslick');
    }
};