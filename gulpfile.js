const gulp = require('gulp');
const pug = require('gulp-pug');
var spritesmith = require('gulp.spritesmith');
var gulpif = require('gulp-if');
var sftp = require('gulp-sftp');

var config = {
    sftp: {
        host: 'html.dev-bitrix.by',
        user: 'Shtml',
        pass: 'eig3SaiZ',
        remotePath: "/home/hdd/html/htdocs/Dubov_K/АРТПРОФ/"
    }
}

gulp.task('deploy', function () {
    return gulp.src('dist/**/*')
        .pipe(sftp(config.sftp));
});

gulp.task('sprite', function () {
    var spriteData = gulp.src('src/img/*.{png,jpg}').pipe(spritesmith({
        imgName: 'sprite.png',
        cssName: 'sprite.css'
    }));
    return spriteData.pipe(gulpif('*.png', gulp.dest('dist/img/'), gulp.dest('src/scss/')));
});

gulp.task('pug', function () {
    return gulp.src(['src/pug/pages/**/*.pug', '!./node_modules/**'])
        .pipe(pug({
            pretty: true
        }))
        .pipe(gulp.dest('dist'));
});

gulp.task('watch', ['pug'], function () {
    gulp.watch('src/**/*', ['pug']);
    gulp.watch("dist/**/*", function (obj) {
        if (["changed", "added"].indexOf(obj.type) !== -1) {
            gulp.src(obj.path, {
                    "base": "dist/"
                })
                .pipe(sftp(config.sftp));
        }
    });
});

gulp.task('clear', function () {
    return cache.clearAll();
})

gulp.task('default', ['watch']);